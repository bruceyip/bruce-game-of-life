let unitLength = 10;
let boxColor = 225;
let strokeColor = 51;
let stableColors = 'blue'
let boxBackground = "black";
let columns; /* To be determined by window width*/
let rows; /* To be determined by window height */
let currentBoard;
let nextBoard;



let slider = document.querySelector(".slider")
document.querySelector(".pause").addEventListener('click', function() {
    noLoop()
})
document.querySelector(".restart").addEventListener('click', function() {
    loop()
})

document.querySelector('.reset')
    .addEventListener('click', function() {
        init()
    });


document.querySelector('.start')
    .addEventListener('click', function() {
        randomInit()
    });

let ruleLoneness = document.querySelector(".rule-loneness")
let ruleOver = document.querySelector(".rule-over")
let reproduction = document.querySelector(".reproduction")



function light() {
    boxBackground = 'grey';
    unitLength = 5;
    color1 = 'blue'
    color2 = 'black'
    color3 = 'navy'
    canvas = createCanvas(windowWidth - windowWidth % unitLength - 100, windowHeight - windowHeight % unitLength - 400);
    // console.log(windowHeight)
    canvas.parent(document.querySelector('#canvas'));
    setup()

}

function dark() {
    boxBackground = 'black';
    unitLength = 10;
    color1 = 'red'
    color2 = 'orange'
    color3 = 'pink'
    canvas = createCanvas(windowWidth - windowWidth % unitLength - 100, windowHeight - windowHeight % unitLength - 400);
    // console.log(windowHeight)
    canvas.parent(document.querySelector('#canvas'));
    setup()

}


function setup() {
    console.log("setup")
        /* Set the canvas to be under the element #canvas*/
    let canvas = createCanvas(windowWidth - windowWidth % unitLength - 100, windowHeight - windowHeight % unitLength - 300);
    // console.log(windowHeight)
    canvas.parent(document.querySelector('#canvas'));

    /*Calculate the number of columns and rows */
    columns = floor(width / unitLength);
    rows = floor(height / unitLength);

    /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
    currentBoard = [];
    nextBoard = [];
    keyboard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = [];

    }
    // Now both currentBoard and nextBoard are array of array of undefined values.
    init(); // Set the initial values of the currentBoard and nextBoard 
}



function randomInit() {
    ruleLoneness.value = 2;
    ruleOver.value = 3;
    reproduction.value = 3;
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = random() > 0.3 ? 1 : 0;
            nextBoard[i][j] = 0;

        }
    }
}

function init() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 1;
            nextBoard[i][j] = 0;

        }
    }
}

let color1 = 'red'
let color2 = 'orange'
let color3 = 'pink'

function randomColor() {
    let randomNum = Math.floor(Math.random() * 3)
    if (randomNum == 0) {
        return color1
    } else if (randomNum == 1) {
        return color2
    } else if (randomNum == 2) {
        return color3
    }
}




function draw() {
    console.log("draw")
    frameRate(parseInt(slider.value))
    background(150);
    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {
                fill(randomColor())
                if (nextBoard[i][j] == currentBoard[i][j]) {
                    fill("lightblue")
                }
            } else {
                fill(boxBackground);
            }

            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
    return setup()
}





function generate() {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of[-1, 0, 1]) {
                for (let j of[-1, 0, 1]) {
                    if (i === 0 && j === 0) {
                        // the cell itself is not its own neighbor
                        continue;

                    }
                    // The modulo operator is crucial for wrapping on the edge
                    neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];

                }
            }


            // Rules of Life
            if (currentBoard[x][y] == 1 && neighbors < ruleLoneness.value) {

                // Die of Loneliness
                nextBoard[x][y] = 0;

            } else
            if (currentBoard[x][y] == 1 && neighbors > ruleOver.value) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;

            } else
            if (currentBoard[x][y] == 0 && neighbors == reproduction.value) {
                // New life due to Reproduction
                nextBoard[x][y] = 1;


            } else {
                // Stasis
                nextBoard[x][y] = currentBoard[x][y];

            }
        }
    }

    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}

document.querySelector('.white')
    .addEventListener('click', function() {
        light()
    });

document.querySelector('.black')
    .addEventListener('click', function() {
        dark()
    });

function mouseDragged() {
    /**
     * If the mouse coordinate is outside the board
     */
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    fill(boxColor);
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

/**
 * When mouse is pressed
 */
function mousePressed() {
    noLoop();
    mouseDragged();
}

/**
 * When mouse is released
 */
function mouseReleased() {
    loop();
}


function mouseDragged() {
    /**
     * If the mouse coordinate is outside the board
     */
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    fill('grey');
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

function mouseClicked() {
    /**
     * If the mouse coordinate is outside the board
     */
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);

    currentBoard[x + one][y] = 1;
    currentBoard[x][y + one] = 1;
    currentBoard[x][y + two] = 1;
    currentBoard[x + one][y + two] = 1;
    currentBoard[x + two][y + two] = 1;
    rect(x * unitLength, y * unitLength, unitLength);
}


let one;
let two;
document.querySelector('.pattern-A')
    .addEventListener('click', function() {
        one = 1;
        two = 2;
        ruleLoneness.value = 2;
        ruleOver.value = 3;
        reproduction.value = 3;
        mouseClicked()

    });

document.querySelector('.pattern-B')
    .addEventListener('click', function() {


        ruleLoneness.value = 0;
        ruleOver.value = 7;
        reproduction.value = 7;


        currentBoard[4][1] = 1;
        currentBoard[4][2] = 1;
        currentBoard[4][3] = 1;
        currentBoard[4][4] = 1;
        currentBoard[4][5] = 1;
        currentBoard[4][6] = 1;
        currentBoard[4][7] = 1;
        currentBoard[7][1] = 1;
        currentBoard[7][2] = 1;
        currentBoard[7][3] = 1;
        currentBoard[7][4] = 1;
        currentBoard[7][5] = 1;
        currentBoard[7][6] = 1;
        currentBoard[7][7] = 1;
        currentBoard[5][1] = 1;
        currentBoard[6][1] = 1;
        currentBoard[5][4] = 1;
        currentBoard[6][4] = 1;
        currentBoard[5][7] = 1;
        currentBoard[6][7] = 1;

        currentBoard[9][1] = 1;
        currentBoard[9][2] = 1;
        currentBoard[9][3] = 1;
        currentBoard[9][4] = 1;
        currentBoard[9][5] = 1;
        currentBoard[9][6] = 1;
        currentBoard[9][7] = 1;
        currentBoard[12][1] = 1;
        currentBoard[12][2] = 1;
        currentBoard[12][3] = 1;
        currentBoard[12][4] = 1;
        currentBoard[12][7] = 1;
        currentBoard[10][1] = 1;
        currentBoard[11][1] = 1;
        currentBoard[10][4] = 1;
        currentBoard[11][4] = 1;
        currentBoard[10][5] = 1;
        currentBoard[11][6] = 1;

        currentBoard[14][1] = 1;
        currentBoard[14][2] = 1;
        currentBoard[14][3] = 1;
        currentBoard[14][4] = 1;
        currentBoard[14][5] = 1;
        currentBoard[14][6] = 1;
        currentBoard[14][7] = 1;
        currentBoard[17][1] = 1;
        currentBoard[17][2] = 1;
        currentBoard[17][3] = 1;
        currentBoard[17][4] = 1;
        currentBoard[17][5] = 1;
        currentBoard[17][6] = 1;
        currentBoard[17][7] = 1;
        currentBoard[15][7] = 1;
        currentBoard[16][7] = 1;

        currentBoard[19][1] = 1;
        currentBoard[19][2] = 1;
        currentBoard[19][3] = 1;
        currentBoard[19][4] = 1;
        currentBoard[19][5] = 1;
        currentBoard[19][6] = 1;
        currentBoard[19][7] = 1;
        currentBoard[20][1] = 1;
        currentBoard[21][1] = 1;
        currentBoard[22][1] = 1;
        currentBoard[20][7] = 1;
        currentBoard[21][7] = 1;
        currentBoard[22][7] = 1;

        currentBoard[19][1] = 1;
        currentBoard[19][2] = 1;
        currentBoard[19][3] = 1;
        currentBoard[19][4] = 1;
        currentBoard[19][5] = 1;
        currentBoard[19][6] = 1;
        currentBoard[19][7] = 1;
        currentBoard[20][1] = 1;
        currentBoard[21][1] = 1;
        currentBoard[22][1] = 1;
        currentBoard[20][7] = 1;
        currentBoard[21][7] = 1;
        currentBoard[22][7] = 1;

        currentBoard[24][1] = 1;
        currentBoard[24][2] = 1;
        currentBoard[24][3] = 1;
        currentBoard[24][4] = 1;
        currentBoard[24][5] = 1;
        currentBoard[24][6] = 1;
        currentBoard[24][7] = 1;
        currentBoard[25][1] = 1;
        currentBoard[26][1] = 1;
        currentBoard[27][1] = 1;
        currentBoard[25][4] = 1;
        currentBoard[26][4] = 1;
        currentBoard[27][4] = 1;
        currentBoard[25][7] = 1;
        currentBoard[26][7] = 1;
        currentBoard[27][7] = 1;

        mouseClicked()

    });

document.querySelector('.pattern-C')
    .addEventListener('click', function() {
        ruleLoneness.value = 2;
        ruleOver.value = 3;
        reproduction.value = 3;

        currentBoard[0][4] = 1;
        currentBoard[0][5] = 1;
        currentBoard[1][4] = 1;
        currentBoard[1][5] = 1;

        currentBoard[10][4] = 1;
        currentBoard[10][5] = 1;
        currentBoard[10][6] = 1;
        currentBoard[11][3] = 1;
        currentBoard[11][7] = 1;
        currentBoard[12][2] = 1;
        currentBoard[13][2] = 1;
        currentBoard[12][8] = 1;
        currentBoard[13][8] = 1;
        currentBoard[14][5] = 1;
        currentBoard[15][3] = 1;
        currentBoard[15][7] = 1;
        currentBoard[16][4] = 1;
        currentBoard[16][5] = 1;
        currentBoard[16][6] = 1;
        currentBoard[17][5] = 1;

        currentBoard[20][2] = 1;
        currentBoard[20][3] = 1;
        currentBoard[20][4] = 1;
        currentBoard[21][2] = 1;
        currentBoard[21][3] = 1;
        currentBoard[21][4] = 1;
        currentBoard[22][1] = 1;
        currentBoard[22][5] = 1;
        currentBoard[24][0] = 1;
        currentBoard[24][1] = 1;
        currentBoard[24][5] = 1;
        currentBoard[24][6] = 1;

        currentBoard[34][2] = 1;
        currentBoard[34][3] = 1;
        currentBoard[35][2] = 1;
        currentBoard[35][3] = 1;




        mouseClicked()

    });






// let storeNumber = `........................X...........................................................................
// ......................X.X...........................................................................
// ............XX......XX............XX................................................................
// ...........X...X....XX............XX................................................................
// XX........X.....X...XX..............................................................................
// XX........X...X.XX....X.X...........................................................................
// ..........X.....X.......X...........................................................................
// ...........X...X....................................................................................
// ............XX......................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................
// ....................................................................................................`


// for (row of rows) {
//     var codes = row.split("")
// }

// console.log(storeNumber)



// Keyboard setting

let xKey = 0;
let yKey = 0;
let remover;

function keyPressed(event) {
    console.log("Keyboard")
    if (keyCode === DOWN_ARROW) {
        yKey = yKey + 1
            // currentBoard[xKey][yKey] = 1;
        fill('white');
        stroke(strokeColor);
        rect(xKey * unitLength, yKey * unitLength, unitLength, unitLength);

    } else
    if (keyCode === UP_ARROW) {
        yKey = yKey - 1
            // currentBoard[xKey][yKey] = 1;
        fill('white');
        stroke(strokeColor);
        rect(xKey * unitLength, yKey * unitLength, unitLength, unitLength)
    } else
    if (keyCode === 39) {
        xKey = xKey + 1
            // currentBoard[xKey][yKey] = 1;
        fill('white');
        stroke(strokeColor);
        rect(xKey * unitLength, yKey * unitLength, unitLength, unitLength)
    } else
    if (keyCode === 37) {
        xKey = xKey - 1
            // currentBoard[xKey][yKey] = 1;
        fill('white');
        stroke(strokeColor);
        rect(xKey * unitLength, yKey * unitLength, unitLength, unitLength)
    } else
    if (keyCode === 32) {
        currentBoard[xKey][yKey] = 1;
        fill('green');
        stroke(strokeColor);
        rect(xKey * unitLength, yKey * unitLength, unitLength, unitLength)
    } else {
        return false; // prevent default}
    }
}

// --pop up message 
const openModalButtons = document.querySelectorAll('[data-modal-target]')
const closeModalButtons = document.querySelectorAll('[data-close-button]')
const overlay = document.getElementById('overlay')

openModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = document.querySelector(button.dataset.modalTarget)
        openModal(modal)
    })
})

overlay.addEventListener('click', () => {
    const modals = document.querySelectorAll('.modal.active')
    modals.forEach(modal => {
        closeModal(modal)
    })
})

closeModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = button.closest('.modal')
        closeModal(modal)
    })
})

function openModal(modal) {
    if (modal == null) return
    modal.classList.add('active')
    overlay.classList.add('active')
}

function closeModal(modal) {
    if (modal == null) return
    modal.classList.remove('active')
    overlay.classList.remove('active')
}

// --pop up message

// ---Q&A

$(function() {

    function setHeight() {
        $(".response").each(function(index, element) {
            var target = $(element);
            target.removeClass("fixed-height");
            var height = target.innerHeight();
            target.attr("data-height", height)
                .addClass("fixed-height");
        });
    };

    $("input[name=question]").on("change", function() {
        $("p.response").removeAttr("style");

        var target = $(this).next().next();
        target.height(target.attr("data-height"));
    })

    setHeight();
});